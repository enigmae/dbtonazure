#######################
## 
## This script is used for login to azure using a service principal.
## The service principal will be used to deploy the resources into azure
##
#######################

export ARM_CLIENT_ID=__AZURE_SERVICE_PRINCIPAL__
export ARM_CLIENT_SECRET=__SERVICE_PRINCIPAL_SECRET--
export ARM_SUBSCRIPTION_ID=--AZURE SUBSCRIPTION--
export ARM_TENANT_ID=--AZURE TENANT ID__

export TF_VAR_subscription_id=$ARM_SUBSCRIPTION_ID
export TF_VAR_spsecret="$ARM_CLIENT_SECRET"

export AZURE_CLIENT_ID=$ARM_CLIENT_ID
export AZURE_CLIENT_SECRET=$ARM_CLIENT_SECRET
export AZURE_TENANT_ID=$ARM_TENANT_ID
export AZURE_SUBSCRIPTION_ID=$ARM_SUBSCRIPTION_ID

az login --service-principal -u $ARM_CLIENT_ID -p $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID
